// BulletTest1.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include "SDL.h"
#include "SDL_opengl.h"
#include "btBulletDynamicsCommon.h"
#include "BulletCollision\Gimpact\btGImpactShape.h"
#include "LinearMath\btIDebugDraw.h"
#include "BulletCollision/Gimpact/btGImpactCollisionAlgorithm.h"

class MyWorld {
public:
	btRigidBody                ** rigidBodies;
	btTriangleIndexVertexArray ** vertexArrays;
	size_t                        num_bodies;
	btDiscreteDynamicsWorld *     m_pDynamicsWorld;
};

void Display_InitGL()
{
    glShadeModel( GL_SMOOTH );
    glClearColor( 0.0f, 0.0f, 0.0f, 0.0f );
    glClearDepth( 1.0f );
    glEnable( GL_DEPTH_TEST );
    glDepthFunc( GL_LEQUAL );
    glHint( GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST );
}

void perspectiveGL( GLdouble fovY, GLdouble aspect, GLdouble zNear, GLdouble zFar )
{
    const GLdouble pi = 3.14159;
    GLdouble fW, fH;
    fH = tan( fovY / 360 * pi ) * zNear;
    fW = fH * aspect;
    glFrustum( -fW, fW, -fH, fH, zNear, zFar );
}

/* function to reset our viewport after a window resize */
void Display_SetViewport( int width, int height )
{
    GLdouble ratio;
    if ( height == 0 )
        height = 1;
    ratio = ( GLdouble )width / ( GLdouble )height;
    glViewport( 0, 0, ( GLsizei )width, ( GLsizei )height );
    glMatrixMode( GL_PROJECTION );
    glLoadIdentity( );
    perspectiveGL( 45.0, ratio, 0.1, 1000);

    // done setting up projection matrix. switch over to model view.
    glMatrixMode( GL_MODELVIEW );
    glLoadIdentity( );
}

SDL_Window* displayWindow;
bool done = false;

btScalar shipMass = 10.0f;
btScalar emptyMass = 2.0f;
btScalar initialFuel = shipMass - emptyMass;
btScalar fuel = initialFuel;
bool thrusterOn = false;
btScalar thrust = 0.0f;

bool pitchForward = false;
bool pitchAft = false;
btScalar pitchForce = 0.0f;

bool yawLeft = false;
bool yawRight = false;
btScalar yawForce = 0.0f;

bool rollCW = false;
bool rollCCW = false;
btScalar rollForce = 0.0f;

bool sas_on = true;

bool isRoll = false;
bool isYaw = false;
bool isPitch = false;

double ticks_per_sim_sec = 20;
double ticks_per_second = 20;

void process_events(MyWorld & myWorld) {
	SDL_Event event;

	Sint16 value;
	int axis;

	while(SDL_PeepEvents(&event, 1, SDL_GETEVENT, SDL_FIRSTEVENT, SDL_LASTEVENT)) {
		switch(event.type) {
			case SDL_KEYDOWN:
				if ( event.key.keysym.sym == SDLK_ESCAPE ) { done = true; }
				else if (event.key.keysym.sym == SDLK_UP) { thrusterOn = true; thrust = 1.0f;}
				else if (event.key.keysym.sym == SDLK_w) { pitchForward = true; pitchForce = -1.0f; }
				else if (event.key.keysym.sym == SDLK_s) { pitchAft = true; pitchForce = 1.0f; }
				else if (event.key.keysym.sym == SDLK_a) { yawLeft = true; yawForce = 1.0f; }
				else if (event.key.keysym.sym == SDLK_d) { yawRight = true; yawForce = -1.0f; }
				else if (event.key.keysym.sym == SDLK_q) { rollCCW = true; rollForce = 1.0f; }
				else if (event.key.keysym.sym == SDLK_e) { rollCW = true; rollForce = -1.0f; }
				else if (event.key.keysym.sym == SDLK_t) { sas_on = !sas_on; }
				break;

			case SDL_KEYUP:
				if(event.key.keysym.sym == SDLK_UP) { thrusterOn = false; }
				else if(event.key.keysym.sym == SDLK_w) { pitchForward = false; pitchForce = 0.0f; }
				else if(event.key.keysym.sym == SDLK_s) { pitchAft = false; pitchForce = 0.0f; }
				else if(event.key.keysym.sym == SDLK_a) { yawLeft = false; yawForce = 0.0f; }
				else if(event.key.keysym.sym == SDLK_d) { yawRight = false; yawForce = 0.0f; }
				else if (event.key.keysym.sym == SDLK_q) { rollCCW = false; rollForce = 0.0f; }
				else if (event.key.keysym.sym == SDLK_e) { rollCW = false; rollForce = 0.0f; }

			case SDL_WINDOWEVENT:
				if(event.window.event == SDL_WINDOWEVENT_CLOSE) { done = true; }
				break;

			case SDL_JOYAXISMOTION:  /* Handle Joystick Motion */
				value = event.jaxis.value;
				axis = event.jaxis.axis;

				if ( (value < -3200) || (value > 3200 ) )
				{
					if( axis == 0) {
						yawLeft = value > 0 ? true : false;
						yawRight = value < 0 ? true : false;
						yawForce = value * -1.0f/32768;
					}
					else if( axis == 1) {
						pitchForward = value > 0 ? true : false;
						pitchAft = value < 0 ? true : false;
						pitchForce = value * 1.0f/32768;
					}
					else if( axis == 2) {
						rollCCW = value > 0 ? true : false;
						rollCW = value < 0 ? true : false;
						rollForce = value * 1.0f/32768;
					}
					else if (axis == 3) {
						thrusterOn = true;
						thrust = (32768-value) * 1.0f/65536;
					}
				}
				else {
					if( event.jaxis.axis == 0 && (yawLeft || yawRight)) {
						yawLeft = yawRight = false;
					}
					else if( event.jaxis.axis == 1 && (pitchAft || pitchForward)) {
						pitchForward = pitchAft = false;
					}
					else if( event.jaxis.axis == 2 && (rollCCW || rollCW)) {
						rollCCW = rollCW = false;
					}
					else if (axis == 3 && thrusterOn) {
						thrusterOn = true;
						thrust = (32768-value) * 1.0f/65536;
					}
				}
				break;

			default:
				break;
		}
	}
}

void init() {
    int width = 1920; //1600;
    int height = 1080; //900;

    SDL_Init(SDL_INIT_VIDEO | SDL_INIT_JOYSTICK);

	SDL_Joystick * joystick;
	SDL_JoystickEventState(SDL_ENABLE);
	joystick = SDL_JoystickOpen(0);

    Uint32 flags = SDL_WINDOW_OPENGL;
    flags |= SDL_WINDOW_FULLSCREEN;
    displayWindow = SDL_CreateWindow("", 50, 50, width, height, flags);
    SDL_GL_CreateContext(displayWindow);
    Display_InitGL();
    Display_SetViewport(width, height);
}

void loop_update_func(MyWorld & myWorld, double dt) {
    SDL_PumpEvents();
	process_events(myWorld);
	// render

	btDiscreteDynamicsWorld * m_pDynamicsWorld = myWorld.m_pDynamicsWorld;

    glClearColor( 0.0f, 0.0f, 0.0f, 0.0f ); // Set the background black
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT ); // Clear The Screen And The Depth Buffer

		glLoadIdentity();

		// fuel bar
		btScalar percent_fuel = fuel/initialFuel;
		btScalar green = 2.0f*percent_fuel;
		btScalar red = 2.0f-2.0f*percent_fuel;
		btClamp<btScalar>(green, 0.0f, 1.0f);
		btClamp<btScalar>(red, 0.0f, 1.0f);
		glColor3f(red, green, 0.0f);
		glBegin(GL_TRIANGLES);
			glVertex3f(-1.0f, 1.4f, -4.0f);
			glVertex3f(-1.0f, 1.3f, -4.0f);
			glVertex3f(-1.0f+percent_fuel*2.0f, 1.4f, -4.0f);

			glVertex3f(-1.0f+percent_fuel*2.0f, 1.4f, -4.0f);
			glVertex3f(-1.0f, 1.3f, -4.0f);
			glVertex3f(-1.0f+percent_fuel*2.0f, 1.3f, -4.0f);
		glEnd();
		glColor3f(1.0f, 1.0f, 1.0f);

		// thrust bar
		glBegin(GL_TRIANGLES);
			glVertex3f(-1.0f, 1.3f, -4.0f);
			glVertex3f(-1.0f, 1.2f, -4.0f);
			glVertex3f(-1.0f+thrust*2.0f, 1.3f, -4.0f);

			glVertex3f(-1.0f+thrust*2.0f, 1.3f, -4.0f);
			glVertex3f(-1.0f, 1.2f, -4.0f);
			glVertex3f(-1.0f+thrust*2.0f, 1.2f, -4.0f);
		glEnd();

		//glTranslatef(0.0f, -6.0f, 0.0f);

		btRigidBody * ship = myWorld.rigidBodies[0];

		int m_maxSubSteps = 10;

		btTransform & shipTransform = ship->getWorldTransform();
		btQuaternion shipRotation = shipTransform.getRotation();
		btVector3 shipAxis = shipRotation.getAxis();

		btScalar shipX = shipTransform.getOrigin().getX();
		btScalar shipY = shipTransform.getOrigin().getY();
		btScalar shipZ = shipTransform.getOrigin().getZ();
		glTranslatef(0.0f, 0.0f, -15.0f);
		glRotatef(30.0f, 1.0f, 0.0f, 0.0f);
		glTranslatef(-shipX, -shipY, -shipZ);

		btVector3 xAxis(1.0f, 0.0f, 0.0f);
		btVector3 yAxis(0.0f, 1.0f, 0.0f);
		btVector3 zAxis(0.0f, 0.0f, 1.0f);
		btVector3 shipXaxis = xAxis.rotate(shipAxis, shipRotation.getAngle());
		btVector3 shipYaxis = yAxis.rotate(shipAxis, shipRotation.getAngle());
		btVector3 shipZaxis = zAxis.rotate(shipAxis, shipRotation.getAngle());

		if(thrusterOn && (fuel > 0.0f))   {
			myWorld.rigidBodies[0]->applyCentralForce(60.0f*thrust*shipYaxis); 

			// burn fuel
			btVector3 inertia;
			btScalar invMass = ship->getInvMass();
			btScalar mass = 1.0f/invMass;
			mass = (mass > emptyMass) ?  mass - 0.01f*thrust : emptyMass;
			fuel = mass - emptyMass;
			ship->getCollisionShape()->calculateLocalInertia( mass, inertia );
			ship->setMassProps( mass, inertia );
			ship->updateInertiaTensor();
		}

		if(pitchForward) { myWorld.rigidBodies[0]->applyTorque(30.0f*pitchForce*shipXaxis); }
		if(pitchAft)     { myWorld.rigidBodies[0]->applyTorque(30.0f*pitchForce*shipXaxis); }
		if(yawLeft)      { myWorld.rigidBodies[0]->applyTorque(30.0f*yawForce*shipZaxis); }
		if(yawRight)     { myWorld.rigidBodies[0]->applyTorque(30.0f*yawForce*shipZaxis); }
		if(rollCW)       { myWorld.rigidBodies[0]->applyTorque(30.0f*rollForce*shipYaxis); }
		if(rollCCW)      { myWorld.rigidBodies[0]->applyTorque(30.0f*rollForce*shipYaxis); }

		if(sas_on) {
			btVector3 angularVel = myWorld.rigidBodies[0]->getAngularVelocity();
			if(angularVel.length2() > 0.0001)
				myWorld.rigidBodies[0]->applyTorque(-10*angularVel.normalized());
		}

		m_pDynamicsWorld->stepSimulation((btScalar)dt, m_maxSubSteps);

		size_t num_bodies = myWorld.num_bodies;

		for(size_t b=0; b<num_bodies; b++) {
			btTransform & btt = myWorld.rigidBodies[b]->getWorldTransform();
			btVector3 & bt_ori = btt.getOrigin();
			glPushMatrix();
			btQuaternion btq = btt.getRotation();
			btVector3 btq_axis = btq.getAxis();
		
			glTranslatef(bt_ori.getX(), bt_ori.getY(), bt_ori.getZ());
			glRotatef(btq.getAngle()*180.0f/3.14f, btq_axis.getX(), btq_axis.getY(), btq_axis.getZ());

			// draw X, Y, and Z vectors
			glBegin(GL_LINES);
				glColor3f(1.0f, 0.0f, 0.0f);
				glVertex3f(0.0f, 0.0f, 0.0f);
				glVertex3f(1.0f, 0.0f, 0.0f);

				glColor3f(0.0f, 1.0f, 0.0f);
				glVertex3f(0.0f, 0.0f, 0.0f);
				glVertex3f(0.0f, 1.0f, 0.0f);

				glColor3f(0.0f, 0.0f, 1.0f);
				glVertex3f(0.0f, 0.0f, 0.0f);
				glVertex3f(0.0f, 0.0f, 1.0f);
			glEnd();

			glColor3f(1.0f, 1.0f, 1.0f);

			IndexedMeshArray & vArr = myWorld.vertexArrays[b]->getIndexedMeshArray();
			btIndexedMesh & im = vArr[0];
			int numTriangles = im.m_numTriangles;
			int * triangleIndexBase = (int *) im.m_triangleIndexBase;
			btScalar * vertexBase = (btScalar *) im.m_vertexBase;

			// draw meshes
			for(int t=0; t<numTriangles; t++) {
				glBegin(GL_LINE_LOOP);
				for(int v=0; v<3; v++) {
					int v_i = triangleIndexBase[t*3+v];
					float v_x = vertexBase[v_i*3];
					float v_y = vertexBase[v_i*3+1];
					float v_z = vertexBase[v_i*3+2];
					glVertex3f(v_x, v_y, v_z);
				}
				glEnd();
			}
			glPopMatrix();
		}

		// draw floor
		for(int i=0; i<200; i++) {
			glBegin(GL_LINES);
				glVertex3f(i-100.0f, 1.0f, -100.0f);
				glVertex3f(i-100.0f, 1.0f, 100.0f);

				glVertex3f(-100.0f, 1.0f, i-100.0f);
				glVertex3f(100.0f, 1.0f, i-100.0f);
			glEnd();
		}

    glFlush();
    SDL_GL_SwapWindow(displayWindow);
}



void loop(MyWorld & myWorld) {

	while(!done) {

		SDL_Delay(1);

        double dt = (1.0/ticks_per_sim_sec);

        Uint32 before = SDL_GetTicks();
		loop_update_func(myWorld, dt);
        Uint32 after = SDL_GetTicks();
        Uint32 elapsed = after - before;
        Uint32 tick_interval = Uint32(1000.0/ticks_per_second);
        bool is_late = elapsed > tick_interval ? true : false;
        Uint32 sleepTime = is_late ? 0 : (tick_interval - elapsed);

        if(sleepTime == 0) {
            std::cout << "WARNING: NO SLEEP TIME" << std::endl;
        }

        if(!is_late) {
            SDL_Delay(sleepTime);
        }

	}

    SDL_Quit();
}

btTriangleIndexVertexArray * make_pyramid() {
	int        	numTriangles        = 6;
	int *      	triangleIndexBase   = new int[3*numTriangles];
	int        	triangleIndexStride = 3*sizeof(int);
	int        	numVertices         = 5;
	btScalar * 	vertexBase          = new btScalar[numVertices*3];
	int        	vertexStride        = 3*sizeof(btScalar);

	btVector3 COM(0.5f, 0.177f, 0.5f);

	vertexBase[0*3+0] = 0.0f - COM.getX(); // x
	vertexBase[0*3+1] = 0.0f - COM.getY(); // y
	vertexBase[0*3+2] = 1.0f - COM.getZ(); // z

	vertexBase[1*3+0] = 1.0f - COM.getX(); // x
	vertexBase[1*3+1] = 0.0f - COM.getY(); // y
	vertexBase[1*3+2] = 1.0f - COM.getZ(); // z

	vertexBase[2*3+0] = 1.0f - COM.getX(); // x
	vertexBase[2*3+1] = 0.0f - COM.getY(); // y
	vertexBase[2*3+2] = 0.0f - COM.getZ(); // z

	vertexBase[3*3+0] = 0.0f - COM.getX(); // x
	vertexBase[3*3+1] = 0.0f - COM.getY(); // y
	vertexBase[3*3+2] = 0.0f - COM.getZ(); // z

	vertexBase[4*3+0] = 0.5f - COM.getX(); // x
	vertexBase[4*3+1] = 0.707f - COM.getY(); // y
	vertexBase[4*3+2] = 0.5f - COM.getZ(); // z

	triangleIndexBase[0*3+0] = 0;
	triangleIndexBase[0*3+1] = 3;
	triangleIndexBase[0*3+2] = 1;

	triangleIndexBase[1*3+0] = 1;
	triangleIndexBase[1*3+1] = 3;
	triangleIndexBase[1*3+2] = 2;

	triangleIndexBase[2*3+0] = 0;
	triangleIndexBase[2*3+1] = 1;
	triangleIndexBase[2*3+2] = 4;

	triangleIndexBase[3*3+0] = 1;
	triangleIndexBase[3*3+1] = 2;
	triangleIndexBase[3*3+2] = 4;

	triangleIndexBase[4*3+0] = 2;
	triangleIndexBase[4*3+1] = 3;
	triangleIndexBase[4*3+2] = 4;

	triangleIndexBase[5*3+0] = 3;
	triangleIndexBase[5*3+1] = 0;
	triangleIndexBase[5*3+2] = 4;

	btTriangleIndexVertexArray * pyramid = new btTriangleIndexVertexArray(numTriangles, triangleIndexBase, triangleIndexStride, numVertices, vertexBase, vertexStride);
	return pyramid;
}

btTriangleIndexVertexArray * make_cube() {

	int        	numTriangles        = 12;
	int *      	triangleIndexBase   = new int[3*numTriangles];
	int        	triangleIndexStride = 3*sizeof(int);
	int        	numVertices         = 8;
	btScalar * 	vertexBase          = new btScalar[numVertices*3];
	int        	vertexStride        = 3*sizeof(btScalar);

	float xs = 1.0f;
	float ys = 1.0f;
	float zs = 1.0f;

	vertexBase[0*3+0] = xs*(0.0f - 0.5f);
	vertexBase[0*3+1] = ys*(0.0f - 0.5f);
	vertexBase[0*3+2] = zs*(0.0f - 0.5f);

	vertexBase[1*3+0] = xs*(1.0f - 0.5f);
	vertexBase[1*3+1] = ys*(0.0f - 0.5f);
	vertexBase[1*3+2] = zs*(0.0f - 0.5f);

	vertexBase[2*3+0] = xs*(1.0f - 0.5f);
	vertexBase[2*3+1] = ys*(0.0f - 0.5f);
	vertexBase[2*3+2] = zs*(1.0f - 0.5f);

	vertexBase[3*3+0] = xs*(0.0f - 0.5f);
	vertexBase[3*3+1] = ys*(0.0f - 0.5f);
	vertexBase[3*3+2] = zs*(1.0f - 0.5f);

	vertexBase[4*3+0] = xs*(0.0f - 0.5f);
	vertexBase[4*3+1] = ys*(1.0f - 0.5f);
	vertexBase[4*3+2] = zs*(0.0f - 0.5f);

	vertexBase[5*3+0] = xs*(1.0f - 0.5f);
	vertexBase[5*3+1] = ys*(1.0f - 0.5f);
	vertexBase[5*3+2] = zs*(0.0f - 0.5f);

	vertexBase[6*3+0] = xs*(1.0f - 0.5f);
	vertexBase[6*3+1] = ys*(1.0f - 0.5f);
	vertexBase[6*3+2] = zs*(1.0f - 0.5f);

	vertexBase[7*3+0] = xs*(0.0f - 0.5f);
	vertexBase[7*3+1] = ys*(1.0f - 0.5f);
	vertexBase[7*3+2] = zs*(1.0f - 0.5f);

	triangleIndexBase[0*3+0] = 0;
	triangleIndexBase[0*3+1] = 1;
	triangleIndexBase[0*3+2] = 3;

	triangleIndexBase[1*3+0] = 1;
	triangleIndexBase[1*3+1] = 2;
	triangleIndexBase[1*3+2] = 3;

	triangleIndexBase[2*3+0] = 0;
	triangleIndexBase[2*3+1] = 4;
	triangleIndexBase[2*3+2] = 1;

	triangleIndexBase[3*3+0] = 1;
	triangleIndexBase[3*3+1] = 4;
	triangleIndexBase[3*3+2] = 5;

	triangleIndexBase[4*3+0] = 0;
	triangleIndexBase[4*3+1] = 7;
	triangleIndexBase[4*3+2] = 4;
	
	triangleIndexBase[5*3+0] = 0;
	triangleIndexBase[5*3+1] = 3;
	triangleIndexBase[5*3+2] = 7;
	
	triangleIndexBase[6*3+0] = 1;
	triangleIndexBase[6*3+1] = 5;
	triangleIndexBase[6*3+2] = 6;
	
	triangleIndexBase[7*3+0] = 1;
	triangleIndexBase[7*3+1] = 6;
	triangleIndexBase[7*3+2] = 2;
	
	triangleIndexBase[8*3+0] = 2;
	triangleIndexBase[8*3+1] = 6;
	triangleIndexBase[8*3+2] = 7;
	
	triangleIndexBase[9*3+0] = 2;
	triangleIndexBase[9*3+1] = 7;
	triangleIndexBase[9*3+2] = 3;
	
	triangleIndexBase[10*3+0] = 4;
	triangleIndexBase[10*3+1] = 7;
	triangleIndexBase[10*3+2] = 5;
	
	triangleIndexBase[11*3+0] = 5;
	triangleIndexBase[11*3+1] = 7;
	triangleIndexBase[11*3+2] = 6;

	btTriangleIndexVertexArray * cube = new btTriangleIndexVertexArray(numTriangles, triangleIndexBase, triangleIndexStride, numVertices, vertexBase, vertexStride);
	return cube;
}

int _tmain(int argc, _TCHAR* argv[])
{

	btDbvtBroadphase                    * m_pBroadphase          = new btDbvtBroadphase();
	btDefaultCollisionConfiguration     * m_pCollisionConfig     = new btDefaultCollisionConfiguration();
	btCollisionDispatcher               * m_pCollisionDispatcher = new btCollisionDispatcher(m_pCollisionConfig);
	
	btGImpactCollisionAlgorithm::registerAlgorithm(m_pCollisionDispatcher); // new

	btSequentialImpulseConstraintSolver * m_pSolver			     = new btSequentialImpulseConstraintSolver();
	btDiscreteDynamicsWorld             * m_pDynamicsWorld       = new btDiscreteDynamicsWorld(m_pCollisionDispatcher, 
																							m_pBroadphase, 
																							m_pSolver, 
																							m_pCollisionConfig);

	float degrees = 0.0f;
	const btQuaternion q0(btVector3(0.0f, 1.0f, 0.0f), btScalar(degrees*3.14f/180.0f));
	const btQuaternion q1(btVector3(1.0f, 0.0f, 0.0f), btScalar(0.0f));
	btTransform initPos0(q0, btVector3(0.0f, 8.0f, -20.0f));
	btTransform initPos1(q1, btVector3(5.0f, 4.0f, -20.0f));
	btTransform initPos2(q1, btVector3(-0.75f, 3.0f, -20.0f));

	btDefaultMotionState motionState0(initPos0);
	btDefaultMotionState motionState1(initPos1);
	btDefaultMotionState motionState2(initPos2);

	btTriangleIndexVertexArray * pyraVertexArray = make_pyramid();
	btGImpactMeshShape pyraShape(pyraVertexArray);
	pyraShape.updateBound();

	btVector3 pyraLocalInertia(0.0f, 0.0f, 0.0f);
	pyraShape.calculateLocalInertia(shipMass, pyraLocalInertia);

	btTriangleIndexVertexArray * cubeVertexArray = make_cube();
	btGImpactMeshShape cubeShape(cubeVertexArray);
	cubeShape.updateBound();

	btVector3 cubeLocalInertia(0.0f, 0.0f, 0.0f);
	cubeShape.calculateLocalInertia(10, cubeLocalInertia);

	btRigidBody::btRigidBodyConstructionInfo info0 ( shipMass, &motionState0, &cubeShape, cubeLocalInertia);
	btRigidBody::btRigidBodyConstructionInfo info1 ( 10, &motionState1, &pyraShape, pyraLocalInertia);
	btRigidBody::btRigidBodyConstructionInfo info2 ( 10, &motionState2, &cubeShape, cubeLocalInertia);

	info0.m_restitution = 0.7f;
	info1.m_restitution = 0.7f;
	info2.m_restitution = 0.7f;

	btRigidBody * pRigidBody0 = new btRigidBody(info1);
	btRigidBody * pRigidBody1 = new btRigidBody(info0);
	btRigidBody * pRigidBody2 = new btRigidBody(info2);

	// this lowers performance a lot but seems necessary for now... (maybe only for slow velocities?)
	pRigidBody0->setActivationState(DISABLE_DEACTIVATION);
	pRigidBody1->setActivationState(DISABLE_DEACTIVATION);
	pRigidBody2->setActivationState(DISABLE_DEACTIVATION);

	MyWorld myWorld;

	myWorld.m_pDynamicsWorld = m_pDynamicsWorld;

	int num_bodies = 3;
	myWorld.num_bodies = num_bodies;
	myWorld.rigidBodies = new btRigidBody * [num_bodies];
	myWorld.rigidBodies[0] = pRigidBody0;
	myWorld.rigidBodies[1] = pRigidBody1;
	myWorld.rigidBodies[2] = pRigidBody2;
	myWorld.vertexArrays = new btTriangleIndexVertexArray * [num_bodies];
	myWorld.vertexArrays[0] = pyraVertexArray;
	myWorld.vertexArrays[1] = cubeVertexArray;
	myWorld.vertexArrays[2] = cubeVertexArray;

	m_pDynamicsWorld->addRigidBody(pRigidBody0);
	m_pDynamicsWorld->addRigidBody(pRigidBody1);
	m_pDynamicsWorld->addRigidBody(pRigidBody2);

	btCollisionShape* groundShape = new btStaticPlaneShape(btVector3(0, 1, 0), 1);
	btDefaultMotionState* groundMotionState = new btDefaultMotionState(btTransform(btQuaternion(0, 0, 0, 1), btVector3(0, 0, 0)));
	btRigidBody::btRigidBodyConstructionInfo groundRigidBodyCI(0, groundMotionState, groundShape, btVector3(0, 0, 0));
	btRigidBody* groundRigidBody = new btRigidBody(groundRigidBodyCI);
	m_pDynamicsWorld->addRigidBody(groundRigidBody);

	const btVector3 gravity(0.0f, -4.0f, 0.0f);
	m_pDynamicsWorld->setGravity(gravity);

	init();

	loop(myWorld);

	return 0;
}

